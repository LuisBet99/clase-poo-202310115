<?php

class F1{
    
    public $Red_Bull = 0;
    private $Mercedes = 0;
    protected $Mclaren = 0;

    function __construct(){
        $this->RedBull = 43;
        $this->Mercedes = 7;
        $this->Mclaren = 9;
    }
    
    function Posiciones(){
        echo "POSICIONES MUNDIAL DE CONSTRUCTORES ANTES DEL GP DE MONACO<br>";
        echo "1. MERCEDES AMG PETRONAS: 126 pts.<br>";
        echo "2. RED BULL RACING: 112 pts.<br>";
        echo "3. MCLAREN MERCEDES: 86 pts.<br>";
    }
    
    public function ptsRedBull(){
        echo "Red Bull obtuvo ".$this->Red_Bull." puntos en el GP de Monaco<br>";
    }

    private function ptsMercedes(){
        echo "Mercedes obtuvo ".$this->Mercedes." puntos en el GP de Monaco<br>";
    }

    public function lcdMercedes(){
        $this->ptsMercedes();
    }

    protected function ptsMclaren(){
        echo "Mclaren obtuvo ".$this->Mclaren." puntos en el GP de Monaco<br>";
    }
    
    public function lcdMclaren(){
        $this->ptsMclaren();
    }

    function __destruct(){
        echo "FIN DEL GP DE MONACO";
    }

}

class F1_2 extends F1{
    
    public $PRedBull = 112;
    public $PMercedes = 126;
    public $PMclaren = 86;
    
    function __construct(){
       
    }
    
    function TSRB(){
        $TRedBull = ($this->PRedBull) + ($this->RedBull);
    }

    function TSM(){
        $TMercedes = ($this->PMercedes) + ($this->Mercedes);
    }
    function TSMC(){
        $TMclaren = ($this->PMclaren) + ($this->Mclaren);
    }

    function Tpts(){
        echo "LA TABLA DE POSICIONES DESPUES DEL GP DE MONACO<br>";
        echo "1. RED BULL: ".$this->TSRB()." pts.<br>";
        echo "2. MERCEDES: ".$this->TSM()." pts.<br>";
        echo "3. MCLAREN: ".$this->TSMC()." pts.<br>"; 
    }

    function __destruct(){

    }
}

$objRedBull = new F1();
$objMercedes = new F1();
$objMclaren = new F1();
$objP = new F1();

$objF1 = new F1_2();
$objRB = new F1_2();
$objM = new F1_2();
$objMC = new F1_2();

$objRedBull->ptsRedBull();
$objMercedes->lcdMercedes();
$objMclaren->lcdMclaren();
$objP->Posiciones();

$objF1->Tpts();
$objRB->TSRB();
$objM->TSM();
$objMC->TSMC();










?>