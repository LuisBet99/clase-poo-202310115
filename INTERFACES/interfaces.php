<?php

interface OperacionesGenerales{

    public function Redondeo($monto);
    public function CalcularIVA($monto);
    public function Infonavit($monto);
    public function LibrasAKilos($monto);
}

class PuntoDeVenta implements OperacionesGenerales{

    public function Redondeo($monto){
        return round($monto);
    }

    public function CalcularIVA($monto){
        return $IVA = $monto * (.16);
    }

    public function Infonavit($monto){
        return $Descuento = $monto * (.05);
    }

    public function LibrasAKilos($monto){
        return $Conversion = $monto / 2.2046;
    }
}



?>