<?php

abstract class Transporte{
    abstract public function mantenimiento();
}

class Avion extends Transporte{
    
    public function mantenimiento(){
        echo "MANTENIMIENTO PREVENTIVO DE UN AVION<br>";
        echo "Revisar Combustible.<br>";
        echo "Retiro, instalación y reparación de neumáticos de tren de aterrizaje y cordones de choque.<br>";
        echo "Servicio a los amortiguadores del tren de aterrizaje, agregando aceite, aire o ambos.<br>";
        echo "Aplicar material conservante a los componentes donde no se requiere el desmontaje de ninguna estructura.<br>";
        echo "Reemplazar ventanas laterales.<br>";
    }
}

class Automovil extends Transporte{
    
    public function mantenimiento(){
        echo "MANTENIMIENTO PREVENTIVO DE UN AUTOMOVL<br>";
        echo "Cambio de filtros y de aceite.<br>";
        echo "Revisión de frenos.<br>";
        echo "Estado de los neumáticos.<br>";
        echo "Integridad de los amortiguadores.<br>";
        echo "Revisión de las luces.<br>";
        echo "La batería y los cables<br>";
    }
}

class Motocicleta extends Transporte{

    public function mantenimiento(){
        echo "MANTENIMIENTO PREVENTIVO DE UNA MOTOCICLETA<br>";
        echo "Vigilar los niveles de líquido refrigerante.<br>";
        echo "Checar los niveles de aceite.<br>";
        echo "Cambiar el filtro de aire y de gasolina.<br>";
        echo "Lubricar las guayas.<br>";
    }
}

class Bicicleta extends Transporte{

    public function mantenimiento(){
        echo "MANTENIMIENTO PREVENTIVO DE UNA BICICLETA<br>";
        echo "Realiza una limpieza a fondo de la bicicleta.<br>";
        echo "Verifica el estado de la dirección, pedales, tornillos, frenos etc.<br>";
        echo "Lubricar la cadena, rodamientos, pivotes, cabecillas de los cables de freno, muelles de los pedales, etc.<br>";
        echo "Revisar el desgaste de los diversos componentes y reemplazarlos de ser necesario.<br>";
    }
}

$obj = new Avion();
$obj->mantenimiento();

$obj2 = new Automovil();
$obj2->mantenimiento();

$obj3 = new Motocicleta();
$obj3->mantenimiento();

$obj4 = new Bicicleta();
$obj4->mantenimiento();







?>