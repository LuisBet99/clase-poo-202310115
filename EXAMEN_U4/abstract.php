<?php

abstract class Figuras{

    abstract public function CalculoArea();
}

class Cuadrado extends Figuras{

    public function CalculoArea(){
        $Lado = 5;
        echo "<h1>El area de un cuadrado que tiene 5cm de lado es: ".$Area = $Lado * $Lado." cms.</h1>";
    }
}

class Rectangulo extends Figuras{

    public function CalculoArea(){
        $Base = 13;
        $Altura = 6;
        echo "<h1>El area de un rectangulo de 13 cm de base y 6cm de altura es: ".$Area = $Base * $Altura." cms.</h1>";
    }
}
$obj = new Cuadrado();
$obj->CalculoArea();

$obj2 = new Rectangulo();
$obj2->CalculoArea();


?>