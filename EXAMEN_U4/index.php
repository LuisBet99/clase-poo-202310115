<?php

class Concatenar{

    public function palabra1(){
        $palabra = "";
        $n = func_num_args();
        $p = func_get_args();
        for ($i=0; $i < $n; $i++) { 
            $palabra .= $p[$i];
        }

        return $palabra;
    }

    public function palabra2(){
        $palabra = "";
        $n = func_num_args();
        $p = func_get_args();
        for ($i=0; $i < $n; $i++) { 
            $palabra .= $p[$i];
        }

        return $palabra;
    }
}

$obj = new Concatenar();
echo "<h1>".$obj->palabra1("Programacion", " Orientada").$obj->palabra2(" A", " Objetos")."</h1>";


?>