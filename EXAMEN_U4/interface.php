<?php

interface F1{

    public function MaxVerstappen($puntos);
    public function LewisHamilton($puntos);
    public function SergioPerez($puntos); 
    public function LandoNorris($puntos);
}

class Clasificacion implements F1{

    public function MaxVerstappen($puntos){
        $total = 0;
        $n = func_num_args();
        $p = func_get_args();
        for ($i=0; $i < $n ; $i++) { 
            $total += $p[$i]; 
        }
        return $total;
    }

    public function LewisHamilton($puntos){
        $total = 0;
        $n = func_num_args();
        $p = func_get_args();
        for ($i=0; $i < $n ; $i++) { 
            $total += $p[$i]; 
        }
        return $total;
    }

    public function SergioPerez($puntos){
        $total = 0;
        $n = func_num_args();
        $p = func_get_args();
        for ($i=0; $i < $n ; $i++) { 
            $total += $p[$i]; 
        }
        return $total;
    }

    public function LandoNorris($puntos){
        $total = 0;
        $n = func_num_args();
        $p = func_get_args();
        for ($i=0; $i < $n ; $i++) { 
            $total += $p[$i]; 
        }
        return $total;
    }
}

$obj = new Clasificacion();
echo "<h1>Clasificacion de pilotos 2021</h1>";
echo "1. Max Verstappen = ".$obj->MaxVerstappen(18, 25, 18, 19, 25, 0)." pts.</h1><br>";
echo "2. Lewis Hamilton = ".$obj->LewisHamilton(25, 19, 25, 25, 7, 0)." pts.</h1><br>";
echo "3. Sergio Perez = ".$obj->SergioPerez(10, 0, 12, 10, 12, 25)." pts.</h1><br>";
echo "4. Lando Norris = ".$obj->LandoNorris(12, 15, 10, 4, 15, 10)." pts.</h1><br>";

?>