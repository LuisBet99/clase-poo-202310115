<?php

class F1{
    //ATRIBUTO PUBLICO
    public $Red_Bull = 43;
    //ATRIBUTO PRIVADO
    private $Mercedes = 18;
    //ATRIBUTO PROTECTED
    protected $Mclaren = 9;

    //METODO PUBLICO
    public function ptsRedBull(){
        echo "Red Bull obtuvo ".$this->Red_Bull." puntos en el GP de Monaco<br>";
    }

    //METODO PRIVADO
    private function ptsMercedes(){
        echo "Mercedes obtuvo ".$this->Mercedes." puntos en el GP de Monaco<br>";
    }

    public function lcdMercedes(){
        $this->ptsMercedes();
    }

    //METODO PROTECTED
    protected function ptsMclaren(){
        echo "Mclaren obtuvo ".$this->Mclaren." puntos en el GP de Monaco<br>";
    }
    
    public function lcdMclaren(){
        $this->ptsMclaren();
    }

}

$objRedBull = new F1();
$objMercedes = new F1();
$objMclaren = new F1();

$objRedBull->ptsRedBull();
$objMercedes->lcdMercedes();
$objMclaren->lcdMclaren();








?>