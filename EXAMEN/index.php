<?php

class Guitarra{

    public $color = "";
    public $tipo = "";
    public $cuerdas = 0;

    public function setcuerdas($No_cuerdas){
        $this->cuerdas = $No_cuerdas;
    }

    public function getcuerdas(){
        return $this->cuerdas;

    }

    public function setcolor($color){
        $this->color = $color;
    }

    public function getcolor(){
        return $this->color;
    }

    public function settipo($tipo){
        $this->tipo = $tipo;
    }

    public function gettipo(){
        return $this->tipo;
    }

}

$objTakamine = new Guitarra ();

$objTakamine->setcolor(Natural);
$objTakamine->settipo(Electroacustica);
$objTakamine->setcuerdas(12);

echo "Color Guitarra :". $objTakamine->getcolor()."<br>";
echo "Tipo de Guitarra :". $objTakamine->gettipo()."<br>";
echo "Numero de Cuerdas :". $objTakamine->getcuerdas()."<br>";

?>
