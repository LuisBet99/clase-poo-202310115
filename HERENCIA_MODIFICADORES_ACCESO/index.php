<?php

class EjemploModificadores{

    public function Metodo1(){
        echo "Este es el metodo 1<br>";
    }

    private function Metodo2(){
        echo "Este es el metodo 2<br>";
    }

    protected function Metodo3(){
        echo "Este es el metodo 3<br>";
    }

    public function AccesoMetodo2(){
        $this->Metodo2();
    }
}

class Hijo extends EjemploModificadores{

    public function AccesoMetodo3(){
        $this->Metodo3();
    }

}

$obj2 = new EjemploModificadores();
$obj = new Hijo();
$obj->Metodo1();
$obj2->AccesoMetodo2();
$obj->AccesoMetodo3();













?>